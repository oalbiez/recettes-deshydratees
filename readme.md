[![pipeline status](https://gitlab.com/oalbiez/recettes-deshydratees/badges/master/pipeline.svg)](https://gitlab.com/oalbiez/recettes-deshydratees/commits/master)

# Recettes Deshydratées


[Toutes les recettes](https://gitlab.com/oalbiez/recettes-deshydratees/builds/artifacts/master/file/book.pdf?job=recettes)



## Mesurer l'himidité résiduelle

1. Peser l'aliment
2. Passer au four à 105° pendant 5H
3. Peser l'aliment

La différence correspond à l'humidité résiduelle.


## Durée de conservation

1. Pour des aliment qui n'ont plus d'activités bacteriennes
Max 3ans pour conserve des valeurs nutritionelles, en général 2ans.
Au delà, on conserve une partie de l'energie, mais on perds les vitamines.

2. Pour des aliments qui ont des activité bacteriennes
28 jours maximum.


## Energies

- flocon de pomme de terre : 3,59 kcal/g
- lait en poudre demi-écrémé : 4,21 kcal/g
- protéines de soja fines : 3,14 kcal/g
- oignons frits : 5,9 kcal/g
- paprika : 3,19 kcal/g
- bouillon de légumes en poudre : 1,64 kcal/g
- poivrons en poudre : 2,51 kcal/g
- poireaux en poudre : 0,36 kcal/g



## TODO

-
- factoriser ciqual et calnut
- Extraire les ingrédients utilisé dans un pantry (garde-manger)


## Linter

- sel < 1g
- energies entre 500 et 600 kcal
- protéines entre 20 et 30g
- sucre < 15g
- Fibre environ 10g
- acide gras entre 10g et 15g