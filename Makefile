
all: pdf

latex:
	@mkdir -p .latex
	@./book generate latex > .latex/book.tex

pdf:
	@latexmk -f .latex/book

release: pdf
	pdfjam --quiet --a4paper --no-landscape --frame true --nup 2x2 --outfile print.pdf book.pdf

clean:
	@latexmk -C -silent .latex/book
	@rm -rf svg-inkscape .report .latex


## Python part

PYTHON_SCRIPTS = book ingredients my_pantry.py
PYTHON_FILES = $(shell find recipes -type f -name '*.py')

typecheck:
	@mypy --ignore-missing-imports --linecount-report .report $(PYTHON_FILES)

lint:
	@pylint -j 4 $(PYTHON_FILES) $(PYTHON_SCRIPTS)

test:
	@pytest -v

check: typecheck lint test

regenerate:
	@./ingredients export calnut > pantries/calnut_pantry.py
	@./ingredients export ciqual > pantries/ciqual_pantry.py
	@black pantries/calnut_pantry.py pantries/ciqual_pantry.py

reformat:
	@black $(PYTHON_FILES) $(PYTHON_SCRIPTS)

