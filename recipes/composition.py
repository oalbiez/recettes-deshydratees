# -*- coding: utf-8 -*-
from dataclasses import dataclass, fields
from typing import Dict
from prettytable import PrettyTable
from recipes.quantity import Quantity, Q


def compute_vitamin_A(retinol: Quantity, beta_carotene: Quantity) -> Quantity:
    # pylint: disable=invalid-name
    return retinol + (beta_carotene / 12)


@dataclass
class Composition:
    # pylint: disable=invalid-name, too-many-instance-attributes
    reference: Quantity = Q.g(100)
    energy: Quantity = Q.kcal(0)
    fat: Quantity = Q.g(0)
    fa_saturated: Quantity = Q.g(0)
    fa_mono: Quantity = Q.g(0)
    fa_poly: Quantity = Q.g(0)
    carbohydrate: Quantity = Q.g(0)
    sugars: Quantity = Q.g(0)
    polyols: Quantity = Q.g(0)
    starch: Quantity = Q.g(0)
    fibres: Quantity = Q.g(0)
    protein: Quantity = Q.g(0)
    salt: Quantity = Q.g(0)
    cholesterol: Quantity = Q.mg(0)
    water: Quantity = Q.g(0)
    vitamin_A: Quantity = Q.ug(0)
    vitamin_D: Quantity = Q.ug(0)
    vitamin_E: Quantity = Q.mg(0)
    vitamin_K1: Quantity = Q.ug(0)
    vitamin_K2: Quantity = Q.ug(0)
    vitamin_C: Quantity = Q.mg(0)
    vitamin_B1: Quantity = Q.mg(0)
    vitamin_B2: Quantity = Q.mg(0)
    vitamin_B3: Quantity = Q.mg(0)
    vitamin_B5: Quantity = Q.mg(0)
    vitamin_B6: Quantity = Q.mg(0)
    vitamin_B12: Quantity = Q.ug(0)
    vitamin_B9: Quantity = Q.ug(0)
    sodium: Quantity = Q.mg(0)
    magnesium: Quantity = Q.mg(0)
    phosphorus: Quantity = Q.mg(0)
    chloride: Quantity = Q.none()  # Q.mg(0)
    potassium: Quantity = Q.mg(0)
    calcium: Quantity = Q.mg(0)
    manganese: Quantity = Q.mg(0)
    iron: Quantity = Q.mg(0)
    copper: Quantity = Q.mg(0)
    zinc: Quantity = Q.mg(0)
    selenium: Quantity = Q.ug(0)
    iodine: Quantity = Q.ug(0)

    @staticmethod
    def for100g(*args, **kwargs) -> "Composition":
        return Composition(*args, **kwargs)

    def to_dict(self) -> Dict[str, Quantity]:
        return dict((field.name, getattr(self, field.name)) for field in fields(self))

    def table(self) -> str:
        table = PrettyTable(
            [
                "Déclaration nutritionnelle",
                f"Unité de meusure (pour {self.reference.render()})",
            ]
        )
        table.align["Déclaration nutritionnelle"] = "l"
        table.align["Unité de meusure"] = "r"
        table.add_row(["Energie", self.energy.render("kcal")])
        table.add_row(["Matières grasses dont", self.fat.render("g")])
        table.add_row(["  Acides gras saturés", self.fa_saturated.render("g")])
        table.add_row(["  Acides gras mono-insaturés", self.fa_mono.render("g")])
        table.add_row(["  Acides gras poly-insaturés", self.fa_poly.render("g")])
        table.add_row(["Glucides dont", self.carbohydrate.render("g")])
        table.add_row(["  Sucres", self.sugars.render("g")])
        table.add_row(["  Polyols", self.polyols.render("g")])
        table.add_row(["  Amidon", self.starch.render("g")])
        table.add_row(["Fibres alimentaires", self.fibres.render("g")])
        table.add_row(["Protéines", self.protein.render("g")])
        table.add_row(["Sel", self.salt.render("g")])
        table.add_row(["Cholestérol", self.cholesterol.render("mg")])
        table.add_row(["Eau", self.water.render("g")])
        table.add_row(["Vitamine A", self.vitamin_A.render()])
        table.add_row(["Vitamine D", self.vitamin_D.render()])
        table.add_row(["Vitamine E", self.vitamin_E.render()])
        table.add_row(["Vitamine K1", self.vitamin_K1.render()])
        table.add_row(["Vitamine K2", self.vitamin_K2.render()])
        table.add_row(["Vitamine C", self.vitamin_C.render()])
        table.add_row(["Vitamine B1", self.vitamin_B1.render()])
        table.add_row(["Vitamine B2", self.vitamin_B2.render()])
        table.add_row(["Vitamine B3", self.vitamin_B3.render()])
        table.add_row(["Vitamine B5", self.vitamin_B5.render()])
        table.add_row(["Vitamine B6", self.vitamin_B6.render()])
        table.add_row(["Vitamine B12", self.vitamin_B12.render()])
        table.add_row(["Vitamine B9", self.vitamin_B9.render()])
        table.add_row(["Sodium", self.sodium.render()])
        table.add_row(["Magnésium", self.magnesium.render()])
        table.add_row(["Phosphore", self.phosphorus.render()])
        table.add_row(["Chlorure", self.chloride.render()])
        table.add_row(["Potassium", self.potassium.render()])
        table.add_row(["Calcium", self.calcium.render()])
        table.add_row(["Manganèse", self.manganese.render()])
        table.add_row(["Fer", self.iron.render()])
        table.add_row(["Cuivre", self.copper.render()])
        table.add_row(["Zinc", self.zinc.render()])
        table.add_row(["Sélénium", self.selenium.render()])
        table.add_row(["Iode", self.iodine.render()])
        return table.get_string()

    def forQ(self, quantity: Quantity) -> "Composition":
        if self.reference == quantity:
            return self
        ratio = quantity / self.reference
        values = {k: v * ratio for k, v in self.to_dict().items()}
        values["reference"] = quantity
        return Composition(**values)

    def normalize(self) -> "Composition":
        return self.forQ(Q.g(100))

    def dehydrated(self) -> "Composition":
        if self.water <= Q.g(5):
            return self
        values = self.to_dict()
        values["reference"] = self.reference - (self.water - Q.g(5))
        values["water"] = Q.g(0)  # Approximation
        return Composition(**values).normalize()

    def ratioWith(self, reference: "Composition") -> "Composition":
        return Composition(
            **{k: getattr(self, k) / v for k, v in reference.to_dict().items()}
        )

    def __add__(self, other: "Composition") -> "Composition":
        return Composition(
            **{k: v + getattr(self, k) for k, v in other.to_dict().items()}
        )

    def render(self) -> str:
        return (
            f"Composition(energy={self.energy.render('kcal')}"
            f", fat={self.fat.render('g')}"
            f", carbohydrate={self.carbohydrate.render('g')}"
            f", protein={self.protein.render('g')}"
            f", salt={self.salt.render('g')}"
            f", water={self.water.render('g')}"
        )
