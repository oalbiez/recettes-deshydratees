# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import List

from recipes.book import Recipe


@dataclass
class Item:
    recipe: Recipe
    code: str
    desciption: str

    def describe(self) -> str:
        return f"{self.code}:{self.recipe.name}:{self.desciption}"


@dataclass
class Accumulator:
    items: List[Item]

    @staticmethod
    def empty() -> "Accumulator":
        return Accumulator([])

    def add(self, item: Item) -> "Accumulator":
        self.items.append(item)
        return self

    def describe(self) -> str:
        return "\n\n".join(i.describe() for i in self.items)
