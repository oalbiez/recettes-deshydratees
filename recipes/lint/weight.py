# -*- coding: utf-8 -*-
from recipes.book import Recipe
from recipes.lint.accumulator import Accumulator, Item
from recipes.quantity import Quantity
from recipes.lint.linter import Linter


def lint_weight(lower: Quantity, upper: Quantity) -> Linter:
    def lint(recipe: Recipe, accumulator: Accumulator) -> None:
        if recipe.weight() <= lower:
            accumulator.add(
                Item(
                    recipe,
                    "weight-too-light",
                    "La recette est trop légère pour un repas complêt.",
                )
            )
        elif recipe.weight() >= upper:
            accumulator.add(
                Item(
                    recipe,
                    "weight-too-heavy",
                    "La recette est trop lourde pour un repas complêt.",
                )
            )

    return lint
