# -*- coding: utf-8 -*-
from typing import Callable
from recipes.book import Recipe
from recipes.lint.accumulator import Accumulator


Linter = Callable[[Recipe, Accumulator], None]
