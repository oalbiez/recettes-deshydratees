# -*- coding: utf-8 -*-
from recipes.book import Recipe
from recipes.lint.accumulator import Accumulator, Item
from recipes.quantity import Quantity
from recipes.lint.linter import Linter


def lint_energy(lower: Quantity, upper: Quantity) -> Linter:
    def lint(recipe: Recipe, accumulator: Accumulator) -> None:
        energy = recipe.composition().energy
        if energy <= lower:
            accumulator.add(
                Item(
                    recipe,
                    "kcal-too-low",
                    "La recette n'a pas assez d'énergie pour un repas complêt.",
                )
            )
        elif energy >= upper:
            accumulator.add(
                Item(
                    recipe,
                    "kcal-too-high",
                    "La recette a trop d'énergie pour un repas complêt.",
                )
            )

    return lint
