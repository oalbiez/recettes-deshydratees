# -*- coding: utf-8 -*-
from typing import List
from recipes.book import Recipe
from recipes.nutriscore import Nutriscore, NutriscoreAnalyse
from recipes.lint.accumulator import Accumulator, Item
from recipes.lint.linter import Linter


def lint_nutriscore(warn: List[Nutriscore]) -> Linter:
    def linter(recipe: Recipe, accumulator: Accumulator) -> None:
        nutriscore = NutriscoreAnalyse.compute_for_recipe(recipe)
        if nutriscore.score() in warn:
            accumulator.add(Item(recipe, "nutriscore", nutriscore.describe()))

    return linter
