# -*- coding: utf-8 -*-
from recipes.book import Recipe
from recipes.lint.accumulator import Accumulator, Item
from recipes.quantity import Quantity
from recipes.lint.linter import Linter


def lint_salt(limit: Quantity) -> Linter:
    def lint(recipe: Recipe, accumulator: Accumulator) -> None:
        if recipe.composition().salt >= limit:
            accumulator.add(
                Item(
                    recipe,
                    "too-much-salt",
                    "La recette est trop salée.",
                )
            )

    return lint
