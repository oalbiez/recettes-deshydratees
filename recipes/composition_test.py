# -*- coding: utf-8 -*-
import pytest
from recipes.composition import Composition
from recipes.quantity import Q


@pytest.mark.skip
def test_add():
    assert Composition.for100g(energy=Q.kcal(200)) + Composition.for100g(
        energy=Q.kcal(100)
    ) == Composition.for100g(energy=Q.kcal(300))


def test_normalize():
    assert Composition(
        reference=Q.g(100), energy=Q.kcal(20)
    ).normalize().energy == Q.kcal(20)
    assert Composition(
        reference=Q.g(10), energy=Q.kcal(20)
    ).normalize().energy == Q.kcal(200)


def test_dehydrated():
    assert Composition(
        reference=Q.g(100), energy=Q.kcal(30), water=Q.g(55)
    ).dehydrated().energy == Q.kcal(60)


def test_ratio_with():
    reference = Composition.for100g(energy=Q.kcal(2000))
    assert Composition.for100g(energy=Q.kcal(200)).ratioWith(
        reference
    ).energy == Q.scalar(0.1)
