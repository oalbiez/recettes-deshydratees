# -*- coding: utf-8 -*-
from recipes.book import Book, Recipe, Ingredient
from recipes.composition import Composition


def process(book: Book) -> str:
    return "\n\n".join(_process_recipe(r) for r in book.recipes)


def _process_recipe(recipe: Recipe) -> str:
    return (
        f"""
---
title: "{recipe.name}"
poids: {recipe.weight():~}
{_process_composition(recipe.composition())}
---

## Ingrédients
"""
        + "\n".join(_process_ingredient(i) for i in recipe.ingredients)
        + """

## Préparation

Ajouter 40cl d'eau bouillante et attendre 8 minutes.
    """
    )


def _process_ingredient(ingredient: Ingredient) -> str:
    return f"- {ingredient.qty:~} {ingredient.label}"


def _process_composition(composition: Composition) -> str:
    return f"""
VNR:
    energy: {composition.energy:.2f~}
    fat: {composition.fat:.2f~}
    fa_saturated: {composition.fa_saturated:.2f~}
    fa_mono: {composition.fa_mono:.2f~}
    fa_poly: {composition.fa_poly:.2f~}
    carbohydrate: {composition.carbohydrate:.2f~}
    sugars: {composition.sugars:.2f~}
    polyols: {composition.polyols:.2f~}
    starch: {composition.starch:.2f~}
    fibres: {composition.fibres:.2f~}
    protein: {composition.protein:.2f~}
    salt: {composition.salt:.2f~}
    cholesterol: {composition.cholesterol:.2f~}
    water: {composition.water:.2f~}
    vitamin_A: {composition.vitamin_A:.2f~}
    vitamin_D: {composition.vitamin_D:.2f~}
    vitamin_E: {composition.vitamin_E:.2f~}
    vitamin_K1: {composition.vitamin_K1:.2f~}
    vitamin_K2: {composition.vitamin_K2:.2f~}
    vitamin_C: {composition.vitamin_C:.2f~}
    vitamin_B1: {composition.vitamin_B1:.2f~}
    vitamin_B2: {composition.vitamin_B2:.2f~}
    vitamin_B3: {composition.vitamin_B3:.2f~}
    vitamin_B5: {composition.vitamin_B5:.2f~}
    vitamin_B6: {composition.vitamin_B6:.2f~}
    vitamin_B12: {composition.vitamin_B12:.2f~}
    vitamin_B9: {composition.vitamin_B9:.2f~}
    sodium: {composition.sodium:.2f~}
    magnesium: {composition.magnesium:.2f~}
    phosphorus: {composition.phosphorus:.2f~}
    chloride: {composition.chloride:.2f~}
    potassium: {composition.potassium:.2f~}
    calcium: {composition.calcium:.2f~}
    manganese: {composition.manganese:.2f~}
    iron: {composition.iron:.2f~}
    copper: {composition.copper:.2f~}
    zinc: {composition.zinc:.2f~}
    selenium: {composition.selenium:.2f~}
    iodine: {composition.iodine:.2f~}
    """
