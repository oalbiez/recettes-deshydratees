# -*- coding: utf-8 -*-

from recipes.book import Book, Recipe, Ingredient
from recipes.quantity import Quantity


def book(*recipes: Recipe) -> Book:
    return Book(recipes)


def recipe(name: str, *ingredients: Ingredient) -> Recipe:
    return Recipe(name, ingredients)


# pylint: disable=invalid-name
Q = Quantity
