# -*- coding: utf-8 -*-
from recipes.quantity import Quantity


def test_equal():
    assert Quantity.g(5) == Quantity.g(5)
    assert Quantity.g(5) == Quantity.mg(5000)


def test_less_than():
    assert Quantity.g(5) < Quantity.g(6)


def test_less_or_equal():
    assert Quantity.g(5) <= Quantity.g(5)


def test_greater_than():
    assert Quantity.g(5) > Quantity.g(4)


def test_greater_or_equal():
    assert Quantity.g(5) >= Quantity.g(5)


def test_add():
    assert Quantity.g(3) + Quantity.g(7) == Quantity.g(10)


def test_sub():
    assert Quantity.g(10) - Quantity.g(7) == Quantity.g(3)


def test_mul():
    assert Quantity.g(10) * 10 == Quantity.g(100)
    assert Quantity.g(10) * Quantity.scalar(10) == Quantity.g(100)


def test_div():
    assert Quantity.g(100) / 10 == Quantity.g(10)
    assert Quantity.g(100) / Quantity.g(10) == Quantity.scalar(10)
