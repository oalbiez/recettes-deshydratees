# -*- coding: utf-8 -*-
from recipes.book import Book, Recipe
from recipes.lint.weight import lint_weight
from recipes.lint.nutriscore import lint_nutriscore
from recipes.lint.energy import lint_energy
from recipes.lint.salt import lint_salt
from recipes.lint.accumulator import Accumulator
from recipes.nutriscore import Nutriscore
from recipes.quantity import Q


LINTERS = [
    lint_weight(Q.g(140), Q.g(160)),
    lint_energy(Q.kcal(500), Q.kcal(600)),
    lint_salt(Q.g(1)),
    lint_nutriscore([Nutriscore.C, Nutriscore.D, Nutriscore.E]),
]


def analyse(book: Book) -> str:
    accumulator = Accumulator.empty()
    for recipe in book.recipes:
        _analyse(recipe, accumulator)
    return accumulator.describe()


def _analyse(recipe: Recipe, accumulator: Accumulator) -> None:
    for linter in LINTERS:
        linter(recipe, accumulator)
