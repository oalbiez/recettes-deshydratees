# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import Optional
from enum import Enum
from pint import UnitRegistry
from pint.quantity import Quantity as PintQuantity

ureg = UnitRegistry()


class Confidence(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    Z = 26

    @classmethod
    def parse(cls, value: str):
        if value in cls.__members__:
            return cls[value]
        return cls.Z

    @classmethod
    def combine(cls, left: "Confidence", right: "Confidence"):
        if not isinstance(left, cls):
            raise TypeError(f"{left} is not a {cls} value")
        if not isinstance(right, cls):
            raise TypeError(f"{right} is not a {cls} value")
        if left.value >= right.value:
            return left
        return right


@dataclass
class Quantity:
    value: Optional[PintQuantity]
    confidence: Confidence = Confidence.Z

    @staticmethod
    def none() -> "Quantity":
        return Quantity(None)

    @staticmethod
    def missing() -> "Quantity":
        return Quantity(None)

    @staticmethod
    def parse(definition: str) -> "Quantity":
        return Quantity(ureg.Quantity(definition))

    @staticmethod
    def scalar(value: float) -> "Quantity":
        return Quantity(value * ureg.dimensionless)

    @staticmethod
    def g(value: float) -> "Quantity":
        # pylint: disable=invalid-name
        return Quantity(value * ureg("g"))

    @staticmethod
    def cac(value: float) -> "Quantity":
        """Approximation du poids du contenu d'une cuillère à café"""
        return value * 5 * ureg("g")

    @staticmethod
    def cas(value: float) -> "Quantity":
        """Approximation du poids du contenu d'une cuillère à soupe"""
        return value * 15 * ureg("g")

    @staticmethod
    def mg(value: float) -> "Quantity":
        # pylint: disable=invalid-name
        return Quantity(value * ureg("mg"))

    @staticmethod
    def µg(value: float) -> "Quantity":
        # pylint: disable=invalid-name, non-ascii-name
        return Quantity(value * ureg("µg"))

    @staticmethod
    def ug(value: float) -> "Quantity":
        # pylint: disable=invalid-name
        return Quantity(value * ureg("µg"))

    @staticmethod
    def kcal(value: float) -> "Quantity":
        return Quantity(value * ureg("kcal"))

    def with_confidence(self, confidence: Confidence) -> "Quantity":
        return Quantity(self.value, confidence)

    def __add__(self, right: "Quantity") -> "Quantity":
        if self.value is None:
            return right
        if right.value is None:
            return self
        return Quantity(
            self.value + right.value,
            Confidence.combine(self.confidence, right.confidence),
        )

    def __sub__(self, right: "Quantity") -> "Quantity":
        if self.value is None:
            return right
        if right.value is None:
            return self
        return Quantity(
            self.value - right.value,
            Confidence.combine(self.confidence, right.confidence),
        )

    def __mul__(self, right: "Quantity | float") -> "Quantity":
        if self.value is None:
            return self
        if isinstance(right, Quantity):
            return Quantity(
                self.value * right.value,
                Confidence.combine(self.confidence, right.confidence),
            )
        return Quantity(self.value * right, self.confidence)

    def __truediv__(self, right: "Quantity | float") -> "Quantity":
        if self.value is None:
            return self
        if isinstance(right, Quantity):
            if right.value is None or right.magnitude() == 0:
                return Q.none()
            return Quantity(
                self.value / right.value,
                Confidence.combine(self.confidence, right.confidence),
            )
        return Quantity(self.value / right, self.confidence)

    def __round__(self, ndigits: int = 0) -> "Quantity":
        if self.value is None:
            return self
        return Quantity(self.value.__round__(ndigits), self.confidence)

    def __lt__(self, right: "Quantity") -> bool:
        if self.value is None or right.value is None:
            return False
        return self.value < right.value

    def __le__(self, right: "Quantity") -> bool:
        if self.value is None or right.value is None:
            return False
        return self.value <= right.value

    def __gt__(self, right: "Quantity") -> bool:
        if self.value is None or right.value is None:
            return False
        return self.value > right.value

    def __ge__(self, right: "Quantity") -> bool:
        if self.value is None or right.value is None:
            return False
        return self.value >= right.value

    def __eq__(self, right: object) -> bool:
        if not isinstance(right, Quantity):
            return False
        if self.value is None or right.value is None:
            return False
        return self.value == right.value and self.confidence == right.confidence

    def __ne__(self, right: object) -> bool:
        if not isinstance(right, Quantity):
            return False
        if self.value is None or right.value is None:
            return False
        return self.value != right.value or self.confidence != right.confidence

    def __format__(self, *args, **kwargs) -> str:
        if self.value is None:
            return "-"
        return self.value.__format__(*args, **kwargs)

    def to(self, unit: str) -> "Quantity":
        # pylint: disable=invalid-name
        if self.value is None:
            return self
        return Quantity(self.value.to(ureg(unit)), self.confidence)

    def render(self, unit=None) -> str:
        if self.value is None:
            return "-"
        if unit is None:
            return f"{self.value:.2f~#P}"
        return f"{self.value.to(ureg(unit)):.2f~P}"

    def magnitude(self):
        if self.value is None:
            return 0
        return self.value.magnitude

    def units(self):
        if self.value is None:
            return ureg.dimensionless
        return self.value.units


# pylint: disable=invalid-name
Q = Quantity
