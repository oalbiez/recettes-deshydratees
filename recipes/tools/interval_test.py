# -*- coding: utf-8 -*-
from recipes.quantity import Q
from recipes.tools.interval import Interval, LowerBound, UpperBound


def test_lower_bound_parse():
    "LowerBound parse and describe should be symetric"
    assert LowerBound.parse("]inf").describe() == "]inf"
    assert LowerBound.parse("]0 g").describe() == "]0 g"
    assert LowerBound.parse("[10 g").describe() == "[10 g"


def test_lower_bound_is_lower_1():
    "LowerBound.is_lower with infinite bound shoud always returns True"
    assert LowerBound.parse("]inf").is_lower(Q.g(-200))
    assert LowerBound.parse("]inf").is_lower(Q.g(0))


def test_lower_bound_is_lower_2():
    "LowerBound.is_lower with closed bound shoud returns True when value >= bound"
    assert not LowerBound.parse("[0g").is_lower(Q.g(-1))
    assert not LowerBound.parse("[0g").is_lower(Q.g(-0.001))
    assert LowerBound.parse("[0g").is_lower(Q.g(0))
    assert LowerBound.parse("[0g").is_lower(Q.g(0.001))


def test_lower_bound_is_lower_3():
    "LowerBound.is_lower with opened bound shoud returns True when value > bound"
    assert not LowerBound.parse("]0g").is_lower(Q.g(-1))
    assert not LowerBound.parse("]0g").is_lower(Q.g(-0.001))
    assert not LowerBound.parse("]0g").is_lower(Q.g(0))
    assert LowerBound.parse("]0g").is_lower(Q.g(0.001))


def test_upper_bound_is_upper_1():
    "UpperBound.is_upper with infinite bound shoud always returns True"
    assert UpperBound.parse("inf[").is_upper(Q.g(0))
    assert UpperBound.parse("inf[").is_upper(Q.g(200))


def test_upper_bound_is_upper_2():
    "UpperBound.is_upper with closed bound shoud returns True when value <= bound"
    assert UpperBound.parse("10g]").is_upper(Q.g(9))
    assert UpperBound.parse("10g]").is_upper(Q.g(9.999))
    assert UpperBound.parse("10g]").is_upper(Q.g(10))
    assert not UpperBound.parse("10g]").is_upper(Q.g(10.001))


def test_upper_bound_is_upper_3():
    "UpperBound.is_upper with opened bound shoud returns True when value < bound"
    assert UpperBound.parse("10g[").is_upper(Q.g(9))
    assert UpperBound.parse("10g[").is_upper(Q.g(9.999))
    assert not UpperBound.parse("10g[").is_upper(Q.g(10))
    assert not UpperBound.parse("10g[").is_upper(Q.g(10.001))


def test_interval_parse():
    "Interval parse and describe should be symetric"
    assert Interval.parse("[0 g; 10 g]").describe() == "[0 g; 10 g]"
    assert Interval.parse("]0 g; 10 g[").describe() == "]0 g; 10 g["


def test_interval_contains():
    "Interval contains should return True is the value is inside the lower and upper bounds"
    assert Q.g(0) in Interval.parse("[0 g; 10 g]")
    assert Q.g(0) not in Interval.parse("]0 g; 10 g]")
    assert Q.g(10) in Interval.parse("[0 g; 10 g]")
    assert Q.g(10) not in Interval.parse("[0 g; 10 g[")
