# -*- coding: utf-8 -*-
from recipes.quantity import Q
from recipes.tools.scale import Scale


def test_scale_rank():
    "Scale should rank a value according to it's definition"
    scale = Scale.parse("[0 g; 1 g]: 0", "]1 g; 2 g]: 1", "]2 g; 3 g]: 2")
    assert scale.rank(Q.g(0)) == 0
    assert scale.rank(Q.g(0.5)) == 0
    assert scale.rank(Q.g(1)) == 0
    assert scale.rank(Q.g(1.01)) == 1
    assert scale.rank(Q.g(2)) == 1
    assert scale.rank(Q.g(2.01)) == 2
    assert scale.rank(Q.g(3)) == 2


def test_scale_interval_for():
    "Scale interval_for should retrive the interval for the given rank"
    scale = Scale.parse("[0 g; 1 g]: 0", "]1 g; 2 g]: 1", "]2 g; 3 g]: 2")
    assert scale.interval_for(1).describe() == "]1 g; 2 g]"
