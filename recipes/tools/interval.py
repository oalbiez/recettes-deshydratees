# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import Optional
from recipes.quantity import Quantity


@dataclass
class LowerBound:
    bound: Optional[Quantity]
    closed: bool

    @staticmethod
    def parse(definition: str) -> "LowerBound":
        if definition == "]inf":
            return LowerBound(None, closed=False)
        if definition.startswith("["):
            return LowerBound(Quantity.parse(definition[1:]), closed=True)
        if definition.startswith("]"):
            return LowerBound(Quantity.parse(definition[1:]), closed=False)
        raise ValueError(f"Invalid LowerBound: {definition}")

    def is_lower(self, value: Quantity) -> bool:
        if self.bound is None:
            return True
        if self.closed:
            return self.bound <= value
        return self.bound < value

    def describe(self) -> str:
        if self.bound is None:
            return "]inf"
        if self.closed:
            return f"[{self.bound:~}"
        return f"]{self.bound:~}"


@dataclass
class UpperBound:
    bound: Optional[Quantity]
    closed: bool

    @staticmethod
    def parse(definition: str) -> "UpperBound":
        if definition == "inf[":
            return UpperBound(None, closed=False)
        if definition.endswith("]"):
            return UpperBound(Quantity.parse(definition[:-1]), closed=True)
        if definition.endswith("["):
            return UpperBound(Quantity.parse(definition[:-1]), closed=False)
        raise ValueError(f"Invalid UpperBound: {definition}")

    def is_upper(self, value: Quantity) -> bool:
        if self.bound is None:
            return True
        if self.closed:
            return self.bound >= value
        return self.bound > value

    def describe(self) -> str:
        if self.bound is None:
            return "inf["
        if self.closed:
            return f"{self.bound:~}]"
        return f"{self.bound:~}["


@dataclass
class Interval:
    lower: LowerBound
    upper: UpperBound

    @staticmethod
    def parse(definition: str) -> "Interval":
        parts = definition.split(";")
        return Interval(
            LowerBound.parse(parts[0].strip()),
            UpperBound.parse(parts[1].strip()),
        )

    def __contains__(self, value: Quantity) -> bool:
        return self.lower.is_lower(value) and self.upper.is_upper(value)

    def describe(self) -> str:
        return f"{self.lower.describe()}; {self.upper.describe()}"
