# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import List
from recipes.quantity import Quantity
from recipes.tools.interval import Interval


@dataclass
class Partition:
    interval: Interval
    rank: int

    @staticmethod
    def parse(definition: str) -> "Partition":
        parts = definition.split(":")
        return Partition(
            Interval.parse(parts[0].strip()),
            int(parts[1].strip()),
        )

    def __contains__(self, value: Quantity) -> bool:
        return self.interval.__contains__(value)

    def describe(self) -> str:
        return f"{self.interval.describe()}: {self.rank}"


@dataclass
class Scale:
    partitions: List[Partition]

    @staticmethod
    def parse(*definitions: str) -> "Scale":
        return Scale([Partition.parse(definition) for definition in definitions])

    def rank(self, quantity: Quantity) -> int:
        for partition in self.partitions:
            if quantity in partition:
                return partition.rank
        raise ValueError(f"{quantity} cannot be ranked")

    def interval_for(self, rank: int) -> Interval:
        for partition in self.partitions:
            if partition.rank == rank:
                return partition.interval
        raise ValueError(f"{rank} not found in this scale")
