# -*- coding: utf-8 -*-

from dataclasses import dataclass
from enum import Enum
from typing import Sequence, Tuple
from recipes.book import Recipe, Ingredient
from recipes.quantity import Quantity, Q
from recipes.tools.scale import Scale


class Nutriscore(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5


PREDICATE = Ingredient.of_groups("^0201.*", "^0203.*", "^0204.*")


def best_vegetables(recipe: Recipe) -> Sequence[Tuple[Quantity, str]]:
    return list(
        sorted(
            ((i.qty, i.label) for i in recipe.ingredients if PREDICATE(i)), reverse=True
        )
    )


ENERGY = Scale.parse(
    "[0 kJ; 335 kJ]    : 0",
    "]335 kJ; 670 kJ]  : 1",
    "]670 kJ; 1005 kJ] : 2",
    "]1005 kJ; 1340 kJ]: 3",
    "]1340 kJ; 1675 kJ]: 4",
    "]1675 kJ; 2010 kJ]: 5",
    "]2010 kJ; 2345 kJ]: 6",
    "]2345 kJ; 2680 kJ]: 7",
    "]2680 kJ; 3015 kJ]: 8",
    "]3015 kJ; 3350 kJ]: 9",
    "]3350 kJ; inf[    : 10",
)

SUGARS = Scale.parse(
    "[0 g; 4.5 g]  : 0",
    "]4.5 g; 9 g]  : 1",
    "]9 g; 13.5 g] : 2",
    "]13.5 g; 18 g]: 3",
    "]18 g; 22.5 g]: 4",
    "]22.5 g; 27 g]: 5",
    "]27 g; 31 g]  : 6",
    "]31 g; 36 g]  : 7",
    "]36 g; 40 g]  : 8",
    "]40 g; 45 g]  : 9",
    "]45 g; inf[   : 10",
)

SATURATEDFAT = Scale.parse(
    "[0 g; 1 g] : 0",
    "]1 g; 2 g] : 1",
    "]2 g; 3 g] : 2",
    "]3 g; 4 g] : 3",
    "]4 g; 5 g] : 4",
    "]5 g; 6 g] : 5",
    "]6 g; 7 g] : 6",
    "]7 g; 8 g] : 7",
    "]8 g; 9 g] : 8",
    "]9 g; 10 g]: 9",
    "]10 g; inf[: 10",
)

SODIUM = Scale.parse(
    "[0 mg; 90 mg]   : 0",
    "]90 mg; 180 mg] : 1",
    "]180 mg; 270 mg]: 2",
    "]270 mg; 360 mg]: 3",
    "]360 mg; 450 mg]: 4",
    "]450 mg; 540 mg]: 5",
    "]540 mg; 630 mg]: 6",
    "]630 mg; 720 mg]: 7",
    "]720 mg; 810 mg]: 8",
    "]810 mg; 900 mg]: 9",
    "]900 mg; inf[  : 10",
)

FIBRE = Scale.parse(
    "[0 g; 0.9g]   : 0",
    "]0.9 g; 1.9 g]: 1",
    "]1.9 g; 2.8 g]: 2",
    "]2.8 g; 3.7 g]: 3",
    "]3.7 g; 4.7 g]: 4",
    "]4.7 g; inf[  : 5",
)

PROTEIN = Scale.parse(
    "[0g ; 1.6 g] : 0",
    "]1.6g; 3.2 g]: 1",
    "]3.2g; 4.8 g]: 2",
    "]4.8g; 6.4 g]: 3",
    "]6.4g; 8 g]  : 4",
    "]8 g; inf[   : 5",
)

VEGETABLES = Scale.parse(
    "[0; 0.4]  : 0",
    "]0.4; 0.6]: 1",
    "]0.6; 0.8]: 2",
    "]0.8; 1]  : 5",
)


@dataclass
class Criterion:
    rank: int
    raw: Quantity
    scale: Scale
    contributors: Sequence[Tuple[Quantity, str]]

    @staticmethod
    def create(
        raw: Quantity, scale: Scale, contributors: Sequence[Tuple[Quantity, str]]
    ) -> "Criterion":
        return Criterion(scale.rank(raw), raw, scale, contributors)

    def most_contributors(self) -> str:
        return ", ".join(f"{qty:.1f~}: {name}" for qty, name in self.contributors[0:3])

    def explain(self) -> str:
        return f"{self.raw:.2f~} in {self.scale.interval_for(self.rank).describe()}"


@dataclass
class NutriscoreAnalyse:
    energy: Criterion
    sugars: Criterion
    fa_saturated: Criterion
    sodium: Criterion
    vegetables: Criterion
    fibres: Criterion
    protein: Criterion

    @staticmethod
    def compute_for_recipe(recipe: Recipe) -> "NutriscoreAnalyse":
        percent = (
            sum((i.qty for i in recipe.ingredients if PREDICATE(i)), Q.g(0))
            / recipe.weight()
        )
        per100g = recipe.composition().normalize()
        return NutriscoreAnalyse(
            energy=Criterion.create(
                per100g.energy, ENERGY, recipe.contribution_to("energy")
            ),
            sugars=Criterion.create(
                per100g.sugars, SUGARS, recipe.contribution_to("sugars")
            ),
            fa_saturated=Criterion.create(
                per100g.fa_saturated,
                SATURATEDFAT,
                recipe.contribution_to("fa_saturated"),
            ),
            sodium=Criterion.create(
                per100g.sodium, SODIUM, recipe.contribution_to("sodium")
            ),
            vegetables=Criterion.create(percent, VEGETABLES, best_vegetables(recipe)),
            fibres=Criterion.create(
                per100g.fibres, FIBRE, recipe.contribution_to("fibres")
            ),
            protein=Criterion.create(
                per100g.protein, PROTEIN, recipe.contribution_to("protein")
            ),
        )

    def rank(self) -> int:
        return self._negative_rank() - self._positive_rank()

    def _negative_rank(self) -> int:
        return (
            self.energy.rank
            + self.sugars.rank
            + self.fa_saturated.rank
            + self.sodium.rank
        )

    def _positive_rank(self) -> int:
        if self._should_use_protein():
            return self.vegetables.rank + self.fibres.rank + self.protein.rank
        return self.vegetables.rank + self.fibres.rank

    def _should_use_protein(self) -> bool:
        return self._negative_rank() < 11

    def score(self) -> Nutriscore:
        rank = self.rank()
        if rank < 0:
            return Nutriscore.A
        if rank in range(0, 3):
            return Nutriscore.B
        if rank in range(3, 11):
            return Nutriscore.C
        if rank in range(11, 19):
            return Nutriscore.D
        if rank in range(19, 41):
            return Nutriscore.E
        return Nutriscore.E

    def describe(self) -> str:
        report = f"Nutriscore: {self.score().name}\n"
        report += (
            f" Energie            : -{self.energy.rank}"
            f" | {self.energy.explain()} | {self.energy.most_contributors()}\n"
        )
        report += f" Sucres             : -{self.sugars.rank} | {self.sugars.most_contributors()}\n"
        report += f" Acides gras saturés: -{self.fa_saturated.rank} | {self.fa_saturated.most_contributors()}\n"
        report += f" Sodium             : -{self.sodium.rank} | {self.sodium.most_contributors()}\n"
        report += " ------------------------\n"
        report += f" N                  : -{self._negative_rank()}\n"
        report += "\n"
        report += f" Fruits et légumes  : +{self.vegetables.rank} | {self.vegetables.most_contributors()}\n"
        report += f" Fibres             : +{self.fibres.rank} | {self.fibres.most_contributors()}\n"
        if self._should_use_protein():
            report += f" Protéines          : +{self.protein.rank} | {self.protein.most_contributors()}\n"
        report += " ------------------------\n"
        report += f" P                  : +{self._positive_rank()}\n"
        report += "\n"
        report += " ========================\n"
        report += f" total              : {self.rank()}\n"
        return report
