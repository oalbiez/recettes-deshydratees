# -*- coding: utf-8 -*-
from recipes.quantity import Confidence


def test_parse():
    assert Confidence.parse("A") == Confidence.A
    assert Confidence.parse("B") == Confidence.B
    assert Confidence.parse("C") == Confidence.C
    assert Confidence.parse("D") == Confidence.D
    assert Confidence.parse("Z") == Confidence.Z

    assert Confidence.parse("?") == Confidence.Z


def test_combine():
    assert Confidence.combine(Confidence.A, Confidence.A) == Confidence.A
    assert Confidence.combine(Confidence.A, Confidence.B) == Confidence.B
    assert Confidence.combine(Confidence.A, Confidence.C) == Confidence.C
    assert Confidence.combine(Confidence.A, Confidence.D) == Confidence.D
    assert Confidence.combine(Confidence.A, Confidence.Z) == Confidence.Z
    assert Confidence.combine(Confidence.Z, Confidence.A) == Confidence.Z
