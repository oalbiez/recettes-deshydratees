# -*- coding: utf-8 -*-
from recipes.composition import Composition
from recipes.nutriscore import NutriscoreAnalyse, Nutriscore
from recipes.quantity import Q, Quantity
from recipes.book import Ingredient, Recipe


def ingredient(label: str, qty: Quantity, composition: Composition) -> Ingredient:
    return Ingredient(label, qty, "", composition)


def energy(qty: Quantity) -> Ingredient:
    return ingredient("energy", qty, Composition.for100g(energy=Q.kcal(100)).forQ(qty))


def sugar(qty: Quantity) -> Ingredient:
    return ingredient("sugar", qty, Composition.for100g(sugars=Q.g(100)).forQ(qty))


def fat(qty: Quantity) -> Ingredient:
    return ingredient("fat", qty, Composition.for100g(fa_saturated=Q.g(100)).forQ(qty))


def salt(qty: Quantity) -> Ingredient:
    return ingredient(
        "salt", qty, Composition.for100g(salt=Q.g(100), sodium=Q.g(100) / 2.5).forQ(qty)
    )


def fibres(qty: Quantity) -> Ingredient:
    return ingredient("fibres", qty, Composition.for100g(fibres=Q.g(100)).forQ(qty))


def protein(qty: Quantity) -> Ingredient:
    return ingredient("protein", qty, Composition.for100g(protein=Q.g(100)).forQ(qty))


def vegetables(qty: Quantity) -> Ingredient:
    return Ingredient("vegetables", qty, "0201xx", Composition.for100g())


def recipe(*ingredients: Ingredient) -> Recipe:
    return Recipe("", ingredients)


def test_compute_nutriscore_1():
    "Evaluate an A"
    good = recipe(
        energy(Q.g(10)),
        sugar(Q.g(10)),
        fat(Q.g(2)),
        salt(Q.g(0)),
        fibres(Q.g(10)),
        protein(Q.g(2)),
        vegetables(Q.g(20)),
    )
    assert NutriscoreAnalyse.compute_for_recipe(good).score() == Nutriscore.A


def test_compute_nutriscore_2():
    "Evaluate a B"
    good = recipe(
        energy(Q.g(10)),
        sugar(Q.g(10)),
        fat(Q.g(9)),
        salt(Q.g(0)),
        fibres(Q.g(10)),
        protein(Q.g(2)),
        vegetables(Q.g(20)),
    )
    assert NutriscoreAnalyse.compute_for_recipe(good).score() == Nutriscore.B


def test_compute_nutriscore_3():
    "Evaluate a C"
    good = recipe(
        energy(Q.g(10)),
        sugar(Q.g(10)),
        fat(Q.g(9)),
        salt(Q.g(0.5)),
        fibres(Q.g(10)),
        protein(Q.g(2)),
        vegetables(Q.g(20)),
    )
    assert NutriscoreAnalyse.compute_for_recipe(good).score() == Nutriscore.C


def test_compute_nutriscore_4():
    "Evaluate a D"
    good = recipe(
        energy(Q.g(10)),
        sugar(Q.g(10)),
        fat(Q.g(9)),
        salt(Q.g(2)),
        fibres(Q.g(1)),
        protein(Q.g(2)),
        vegetables(Q.g(20)),
    )
    assert NutriscoreAnalyse.compute_for_recipe(good).score() == Nutriscore.D


def test_compute_nutriscore_5():
    "Evaluate a E"
    good = recipe(
        energy(Q.g(10)),
        sugar(Q.g(10)),
        fat(Q.g(20)),
        salt(Q.g(20)),
        fibres(Q.g(1)),
        protein(Q.g(2)),
        vegetables(Q.g(20)),
    )
    assert NutriscoreAnalyse.compute_for_recipe(good).score() == Nutriscore.E
