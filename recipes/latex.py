# -*- coding: utf-8 -*-
from typing import Sequence
from recipes.book import Book, Recipe, Ingredient
from recipes.composition import Composition
from recipes.quantity import Quantity
from recipes.vnr import VNR
from recipes.nutriscore import Nutriscore, NutriscoreAnalyse

Header: str = """
\\documentclass[12pt]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage[french]{babel}
\\usepackage[a4paper,
    left=1cm,
	right=1cm,
	top=1cm,
	bottom=1cm,
	headheight=11pt,
]{geometry}
\\usepackage{colortbl}
\\usepackage{cooking-units}
\\usepackage{fontawesome}
\\usepackage{svg}

\\usepackage[absolute]{textpos}
%\\usepackage[colorgrid,texcoord]{eso-pic}[2002/11/16]

\\textblockorigin{0cm}{0cm}


\\pagenumbering{gobble}

\\newcookingunit[c.à.s]{cas}
\\newcookingunit[c.à.c]{cac}

\\setlength{\\parindent}{0pt}


\\begin{document}

"""

Footer: str = """
\\end{document}
"""


Images = {
    Nutriscore.A: "images/Nutri-score-A.svg",
    Nutriscore.B: "images/Nutri-score-B.svg",
    Nutriscore.C: "images/Nutri-score-C.svg",
    Nutriscore.D: "images/Nutri-score-D.svg",
    Nutriscore.E: "images/Nutri-score-E.svg",
}


def process(book: Book) -> str:
    return Header + "\n".join(_process_recipe(r) for r in book.recipes) + Footer


def _process_recipe(recipe: Recipe) -> str:
    #  \\hfill {_process_quantity(recipe.weight())} -- {_process_quantity(recipe.composition().energy)}
    return (
        f"\\section*{{{recipe.name}}}\n\n"
        + "\\begin{textblock*}{60mm}(140mm,10mm)\n\\begin{center}"
        + f"{_nutriscore(recipe)}\n\n"
        + "\\vspace{5mm}"
        + f"{{\\Huge\\bf{_process_quantity(recipe.weight())}}}\n\n"
        + "\\vspace{2mm}"
        + f"{{\\Huge\\bf{_process_quantity(recipe.composition().energy.to('kJ'))}}}\n\n"
        + "\\vspace{2mm}"
        + f"{{\\Huge\\bf{_process_quantity(recipe.composition().energy)}}}\n\n"
        + "\\end{center}\\end{textblock*}\n\n"
        + "\\subsection*{Ingrédients}\n"
        + _process_ingredients(recipe.ingredients, recipe.weight())
        + "\\subsection*{Préparation}\n"
        + "Ajouter 40cl d'eau bouillante et attendre 8 minutes.\n\n"
        + "\\subsection*{Valeurs nutritionnelles}\n"
        + f"{_process_composition(recipe.composition())}\n\n"
        + "\\newpage\n\n"
    )


def _nutriscore(recipe: Recipe) -> str:
    score = NutriscoreAnalyse.compute_for_recipe(recipe).score()
    image = Images[score]
    return f"\\includesvg[width=4cm]{{{image}}}"


def _process_ingredients(ingredients: Sequence[Ingredient], total: Quantity) -> str:
    return (
        "\\begin{tabular*}{\\textwidth}{ l r l r }\n"
        + "\n".join(_process_ingredient(i, total) for i in ingredients)
        + "\\end{tabular*}\n\n"
    )


def _process_ingredient(ingredient: Ingredient, total: Quantity) -> str:
    return (
        "\\faCaretRight & "
        f"{_process_quantity(ingredient.qty)} &"
        f"{ingredient.label} &"
        f"{_percent(ingredient.qty / total)}\\\\\n"
    )


def _process_quantity(quantity: Quantity) -> str:
    return f"\\cunum{{{quantity.magnitude():.0f}}}{{{quantity.units():~}}}"


def _percent(quantity: Quantity) -> str:
    if quantity.value is None:
        return "-"
    return f"${quantity.magnitude() * 100:.1f}\\%$"


def _process_composition(composition: Composition) -> str:
    ratio = composition.ratioWith(VNR)
    normalized = composition.normalize()
    odd = "\\rowcolor{gray!10!white}"
    even = "\\rowcolor{gray!30!white}"
    return (
        "{\n"
        "\\arrayrulecolor{white}\n"
        "\\setlength\\arrayrulewidth{2pt}\n"
        "\\tiny\n"
        "\\setlength{\\tabcolsep}{3pt}\n"
        "\\begin{tabular*}{\\textwidth}{ l | c | c | c | l | c | c | c | l | c | c | c }\n"
        + _line(
            "\\rowcolor{darkgray}",
            "",
            "\\color{white} Portion",
            "\\color{white} 100 g",
            "\\color{white} VNR",
            "",
            "\\color{white} Portion",
            "\\color{white} 100 g",
            "\\color{white} VNR",
            "",
            "\\color{white} Portion",
            "\\color{white} 100 g",
            "\\color{white} VNR",
        )
        + "\\hline\n"
        + _line(
            odd,
            _item("Energie"),
            _value(composition.energy),
            _value(normalized.energy),
            _percent(ratio.energy),
            _item("Vitamine A"),
            _value(composition.vitamin_A),
            _value(normalized.vitamin_A),
            _percent(ratio.vitamin_A),
            _item("Sodium"),
            _value(composition.sodium),
            _value(normalized.sodium),
            _percent(ratio.sodium),
        )
        + _line(
            even,
            _item("Energie"),
            _value(composition.energy.to("kJ")),
            _value(normalized.energy.to("kJ")),
            _percent(ratio.energy),
            _item("Vitamine D"),
            _value(composition.vitamin_D),
            _value(normalized.vitamin_D),
            _percent(ratio.vitamin_D),
            _item("Magnésium"),
            _value(composition.magnesium),
            _value(normalized.magnesium),
            _percent(ratio.magnesium),
        )
        + _line(
            odd,
            _item("Matières grasses"),
            _value(composition.fat),
            _value(normalized.fat),
            _percent(ratio.fat),
            _item("Vitamine E"),
            _value(composition.vitamin_E),
            _value(normalized.vitamin_E),
            _percent(ratio.vitamin_E),
            _item("Phosphore"),
            _value(composition.phosphorus),
            _value(normalized.phosphorus),
            _percent(ratio.phosphorus),
        )
        + _line(
            even,
            _subitem("Acides gras saturés"),
            _value(composition.fa_saturated),
            _value(normalized.fa_saturated),
            _percent(ratio.fa_saturated),
            _item("Vitamine K1"),
            _value(composition.vitamin_K1),
            _value(normalized.vitamin_K1),
            _percent(ratio.vitamin_K1),
            _item("Chlorure"),
            _value(composition.chloride),
            _value(normalized.chloride),
            _percent(ratio.chloride),
        )
        + _line(
            odd,
            _subitem("Acides gras mono-insaturés"),
            _value(composition.fa_mono),
            _value(normalized.fa_mono),
            _percent(ratio.fa_mono),
            _item("Vitamine K2"),
            _value(composition.vitamin_K2),
            _value(normalized.vitamin_K2),
            _percent(ratio.vitamin_K2),
            _item("Potassium"),
            _value(composition.potassium),
            _value(normalized.potassium),
            _percent(ratio.potassium),
        )
        + _line(
            even,
            _subitem("Acides gras poly-insaturés"),
            _value(composition.fa_poly),
            _value(normalized.fa_poly),
            _percent(ratio.fa_poly),
            _item("Vitamine C"),
            _value(composition.vitamin_C),
            _value(normalized.vitamin_C),
            _percent(ratio.vitamin_C),
            _item("Calcium"),
            _value(composition.calcium),
            _value(normalized.calcium),
            _percent(ratio.calcium),
        )
        + _line(
            odd,
            _item("Glucides"),
            _value(composition.carbohydrate),
            _value(normalized.carbohydrate),
            _percent(ratio.carbohydrate),
            _item("Vitamine B1"),
            _value(composition.vitamin_B1),
            _value(normalized.vitamin_B1),
            _percent(ratio.vitamin_B1),
            _item("Manganèse"),
            _value(composition.manganese),
            _value(normalized.manganese),
            _percent(ratio.manganese),
        )
        + _line(
            even,
            _subitem("Sucres"),
            _value(composition.sugars),
            _value(normalized.sugars),
            _percent(ratio.sugars),
            _item("Vitamine B2"),
            _value(composition.vitamin_B2),
            _value(normalized.vitamin_B2),
            _percent(ratio.vitamin_B2),
            _item("Fer"),
            _value(composition.iron),
            _value(normalized.iron),
            _percent(ratio.iron),
        )
        + _line(
            odd,
            _subitem("Polyols"),
            _value(composition.polyols),
            _value(normalized.polyols),
            _percent(ratio.polyols),
            _item("Vitamine B3"),
            _value(composition.vitamin_B3),
            _value(normalized.vitamin_B3),
            _percent(ratio.vitamin_B3),
            _item("Cuivre"),
            _value(composition.copper),
            _value(normalized.copper),
            _percent(ratio.copper),
        )
        + _line(
            even,
            _subitem("Amidon"),
            _value(composition.starch),
            _value(normalized.starch),
            _percent(ratio.starch),
            _item("Vitamine B5"),
            _value(composition.vitamin_B5),
            _value(normalized.vitamin_B5),
            _percent(ratio.vitamin_B5),
            _item("Zinc"),
            _value(composition.zinc),
            _value(normalized.zinc),
            _percent(ratio.zinc),
        )
        + _line(
            odd,
            _item("Fibres alimentaires"),
            _value(composition.fibres),
            _value(normalized.fibres),
            _percent(ratio.fibres),
            _item("Vitamine B6"),
            _value(composition.vitamin_B6),
            _value(normalized.vitamin_B6),
            _percent(ratio.vitamin_B6),
            _item("Sélénium"),
            _value(composition.selenium),
            _value(normalized.selenium),
            _percent(ratio.selenium),
        )
        + _line(
            even,
            _item("Protéines"),
            _value(composition.protein),
            _value(normalized.protein),
            _percent(ratio.protein),
            _item("Vitamine B9"),
            _value(composition.vitamin_B9),
            _value(normalized.vitamin_B9),
            _percent(ratio.vitamin_B9),
            _item("Iode"),
            _value(composition.iodine),
            _value(normalized.iodine),
            _percent(ratio.iodine),
        )
        + _line(
            odd,
            _item("Sel"),
            _value(composition.salt),
            _value(normalized.salt),
            _percent(ratio.salt),
            _item("Vitamine B12"),
            _value(composition.vitamin_B12),
            _value(normalized.vitamin_B12),
            _percent(ratio.vitamin_B12),
            "",
            "",
            "",
            "",
        )
        + _line(
            even,
            _item("Cholestérol"),
            _value(composition.cholesterol),
            _value(normalized.cholesterol),
            _percent(ratio.cholesterol),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        )
        + _line(
            odd,
            _item("Eau"),
            _value(composition.water),
            _value(normalized.water),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        )
        + "\\end{tabular*}\n"
        + "}\n"
    )


def _line(color: str, *items: str) -> str:
    return f"{color} {'&'.join(items)}\\\\\n"


def _item(name: str) -> str:
    return name


def _subitem(name: str) -> str:
    return f"\\hspace{{2mm}}{name}"


def _bold(value: str) -> str:
    return f"{{\\bf {value}}}"


def _value(quantity: Quantity) -> str:
    return f"${quantity:.1f~L}$"
