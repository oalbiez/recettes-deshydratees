# -*- coding: utf-8 -*-


from typing import Sequence

from recipes.pantry import Aliment
from recipes.composition import Composition
from recipes.quantity import Quantity


_header: str = """
# -*- coding: utf-8 -*-
from receipts.pantry_dsl import pantry, aliment, composition, value, missing


"""


def aliments_to_python(aliments: Sequence[Aliment]) -> str:
    return (
        _header
        + "PANTRY = pantry(\n"
        + ",\n".join(map(_aliment_to_python, aliments))
        + ")"
    )


def _aliment_to_python(aliment: Aliment) -> str:
    return f"""
    aliment(
        identifier={repr(aliment.identifier)},
        label={repr(aliment.label)},
        group={repr(aliment.group)},
        note={repr(aliment.note)},
        composition={_composition_to_python(aliment.composition)})"""


def _composition_to_python(composition: Composition) -> str:
    return f"""composition(
            energy={_quantity_to_python(composition.energy)},
            fat={_quantity_to_python(composition.fat)},
            fa_saturated={_quantity_to_python(composition.fa_saturated)},
            fa_mono={_quantity_to_python(composition.fa_mono)},
            fa_poly={_quantity_to_python(composition.fa_poly)},
            carbohydrate={_quantity_to_python(composition.carbohydrate)},
            sugars={_quantity_to_python(composition.sugars)},
            polyols={_quantity_to_python(composition.polyols)},
            starch={_quantity_to_python(composition.starch)},
            fibres={_quantity_to_python(composition.fibres)},
            protein={_quantity_to_python(composition.protein)},
            salt={_quantity_to_python(composition.salt)},
            cholesterol={_quantity_to_python(composition.cholesterol)},
            water={_quantity_to_python(composition.water)},
            vitamin_A={_quantity_to_python(composition.vitamin_A)},
            vitamin_D={_quantity_to_python(composition.vitamin_D)},
            vitamin_E={_quantity_to_python(composition.vitamin_E)},
            vitamin_K1={_quantity_to_python(composition.vitamin_K1)},
            vitamin_K2={_quantity_to_python(composition.vitamin_K2)},
            vitamin_C={_quantity_to_python(composition.vitamin_C)},
            vitamin_B1={_quantity_to_python(composition.vitamin_B1)},
            vitamin_B2={_quantity_to_python(composition.vitamin_B2)},
            vitamin_B3={_quantity_to_python(composition.vitamin_B3)},
            vitamin_B5={_quantity_to_python(composition.vitamin_B5)},
            vitamin_B6={_quantity_to_python(composition.vitamin_B6)},
            vitamin_B12={_quantity_to_python(composition.vitamin_B12)},
            vitamin_B9={_quantity_to_python(composition.vitamin_B9)},
            sodium={_quantity_to_python(composition.sodium)},
            magnesium={_quantity_to_python(composition.magnesium)},
            phosphorus={_quantity_to_python(composition.phosphorus)},
            chloride={_quantity_to_python(composition.chloride)},
            potassium={_quantity_to_python(composition.potassium)},
            calcium={_quantity_to_python(composition.calcium)},
            manganese={_quantity_to_python(composition.manganese)},
            iron={_quantity_to_python(composition.iron)},
            copper={_quantity_to_python(composition.copper)},
            zinc={_quantity_to_python(composition.zinc)},
            selenium={_quantity_to_python(composition.selenium)},
            iodine={_quantity_to_python(composition.iodine)},
        )
"""


def _quantity_to_python(value: Quantity) -> str:
    if value.value is None:
        return "missing()"
    return f"value('{value:~}')"
