from dataclasses import dataclass
from typing import Optional, List


@dataclass
class Group:
    code: str
    name: Optional[str]

    @staticmethod
    def to_string(*groups: "Group") -> str:
        return (
            "|"
            + "|".join([f"{g.code}: {g.name}" for g in groups if g.name is not None])
            + "|"
        )


def to_note(source: str, code: str, name: str, groups: List[Group]) -> str:
    return f"{name}[{source}][{code}] {Group.to_string(*groups)}"


def to_group(groups: List[Group]) -> str:
    for grp in reversed(groups):
        if int(grp.code) != 0:
            return grp.code
    return ""
