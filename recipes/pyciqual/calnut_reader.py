# -*- coding: utf-8 -*-

from typing import Iterable
from pandas import DataFrame, read_excel
from recipes.quantity import Q
from recipes.composition import Composition, compute_vitamin_A
from recipes.pantry import Aliment, Pantry
from recipes.pyciqual.common import Group, to_note, to_group


def aliments(path: str) -> Pantry:
    return Pantry.create(
        _to_aliment(row) for row in _rows_of(f"{path}/calnut.xlsx", "MB")
    )


def _rows_of(filename: str, hypothesis: str) -> Iterable[DataFrame]:
    excel = read_excel(filename, sheet_name=0, dtype=object)
    for _, row in excel.iterrows():
        if row.at["HYPOTH"] == hypothesis:
            yield row


def _to_aliment(row: DataFrame) -> Aliment:
    groups = [
        Group(row.at["alim_grp_code"], row.at["alim_grp_nom_fr"]),
        Group(row.at["alim_ssgrp_code"], row.at["alim_ssgrp_nom_fr"]),
        Group(row.at["alim_ssssgrp_code"], row.at["alim_ssssgrp_nom_fr"]),
    ]
    return Aliment(
        identifier=row.at["FOOD_LABEL"],
        label=row.at["FOOD_LABEL"],
        group=to_group(groups),
        note=to_note(
            source="calnut",
            code=row.at["alim_code"],
            name=row.at["FOOD_LABEL"],
            groups=groups,
        ),
        composition=_to_composition(row),
    )


def _to_composition(row: DataFrame) -> Composition:
    return Composition(
        energy=Q.kcal(row.at["nrj_kcal"]),
        fat=Q.g(row.at["lipides_g"]),
        fa_saturated=Q.g(row.at["ags_g"]),
        fa_mono=Q.g(row.at["agmi_g"]),
        fa_poly=Q.g(row.at["agpi_g"]),
        carbohydrate=Q.g(row.at["glucides_g"]),
        sugars=Q.g(row.at["sucres_g"]),
        polyols=Q.g(row.at["polyols_g"]),
        starch=Q.g(row.at["amidon_g"]),
        fibres=Q.g(row.at["fibres_g"]),
        protein=Q.g(row.at["proteines_g"]),
        salt=Q.g(row.at["sel_g"]),
        cholesterol=Q.mg(row.at["cholesterol_mg"]),
        water=Q.g(row.at["eau_g"]),
        vitamin_A=compute_vitamin_A(
            Q.µg(row.at["retinol_mcg"]), Q.µg(row.at["beta_carotene_mcg"])
        ),
        vitamin_D=Q.µg(row.at["vitamine_d_mcg"]),
        vitamin_E=Q.mg(row.at["vitamine_e_mg"]),
        vitamin_K1=Q.µg(row.at["vitamine_k1_mcg"]),
        vitamin_K2=Q.µg(row.at["vitamine_k2_mcg"]),
        vitamin_C=Q.mg(row.at["vitamine_c_mg"]),
        vitamin_B1=Q.mg(row.at["vitamine_b1_mg"]),
        vitamin_B2=Q.mg(row.at["vitamine_b2_mg"]),
        vitamin_B3=Q.mg(row.at["vitamine_b3_mg"]),
        vitamin_B5=Q.mg(row.at["vitamine_b5_mg"]),
        vitamin_B6=Q.mg(row.at["vitamine_b6_mg"]),
        vitamin_B12=Q.µg(row.at["vitamine_b12_mcg"]),
        vitamin_B9=Q.µg(row.at["vitamine_b9_mcg"]),
        sodium=Q.mg(row.at["sodium_mg"]),
        magnesium=Q.mg(row.at["magnesium_mg"]),
        phosphorus=Q.mg(row.at["phosphore_mg"]),
        chloride=Q.none(),
        potassium=Q.mg(row.at["potassium_mg"]),
        calcium=Q.mg(row.at["calcium_mg"]),
        manganese=Q.mg(row.at["manganese_mg"]),
        iron=Q.mg(row.at["fer_mg"]),
        copper=Q.mg(row.at["cuivre_mg"]),
        zinc=Q.mg(row.at["zinc_mg"]),
        selenium=Q.µg(row.at["selenium_mcg"]),
        iodine=Q.µg(row.at["iode_mcg"]),
    )
