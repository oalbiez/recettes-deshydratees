# -*- coding: utf-8 -*-
import re

from dataclasses import dataclass
from typing import Callable, Sequence
from typing import Tuple
from recipes.quantity import Quantity, Q
from recipes.composition import Composition


@dataclass
class Ingredient:
    label: str
    qty: Quantity
    group: str
    composition: Composition

    def __repr__(self) -> str:
        return f"Ingredient({self.qty:~}, {self.label}, {self.composition.render()})"

    @staticmethod
    def of_groups(*patterns: str) -> Callable[["Ingredient"], bool]:
        expressions = [re.compile(p) for p in patterns]

        def predicate(ingredient: "Ingredient") -> bool:
            for expression in expressions:
                if re.match(expression, ingredient.group) is not None:
                    return True
            return False

        return predicate


@dataclass
class Recipe:
    name: str
    ingredients: Tuple[Ingredient, ...]

    def weight(self) -> Quantity:
        return sum((i.qty for i in self.ingredients), Q.g(0))

    def composition(self) -> Composition:
        result = Composition(reference=Quantity.g(0))
        for i in self.ingredients:
            result = result + i.composition
        return result

    def contribution_to(self, attribute: str) -> Sequence[Tuple[Quantity, str]]:
        return sorted(
            ((getattr(i.composition, attribute), i.label) for i in self.ingredients),
            reverse=True,
        )

    def __add__(self, other: "Recipe") -> "Recipe":
        return Recipe(self.name + other.name, self.ingredients + other.ingredients)


@dataclass
class Book:
    recipes: Tuple[Recipe, ...]

    def __add__(self, other: "Book") -> "Book":
        return Book(self.recipes + other.recipes)


def book(*recipes: Recipe) -> Book:
    return Book(recipes)


def recipe(name: str, *ingredients: Ingredient) -> Recipe:
    return Recipe(name, ingredients)
