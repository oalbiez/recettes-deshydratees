# -*- coding: utf-8 -*-

from typing import Callable, TypeVar


# pylint: disable=invalid-name
T = TypeVar("T")
# pylint: enable=invalid-name

Predicate = Callable[[T], bool]


def all_true(*predicates: Callable[[T], bool]) -> Callable[[T], bool]:
    def predicate(item: T) -> bool:
        for predicate in predicates:
            if not predicate(item):
                return False
        return True

    return predicate


def one_true(*predicates: Callable[[T], bool]) -> Callable[[T], bool]:
    def predicate(item: T) -> bool:
        for predicate in predicates:
            if predicate(item):
                return True
        return False

    return predicate
