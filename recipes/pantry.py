# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import Callable, Dict, List, Iterable
from recipes.book import Ingredient
from recipes.composition import Composition
from recipes.quantity import Quantity


@dataclass
class Aliment:
    identifier: str
    label: str
    group: str
    note: str
    composition: Composition

    def composition_for(self, qty: Quantity) -> Composition:
        return self.composition.forQ(qty)

    def dehydrated(self) -> "Aliment":
        return Aliment(
            self.identifier + "/dehydrated",
            self.label + "(dehydrated)",
            self.group,
            self.note,
            self.composition.dehydrated(),
        )

    def key(self):
        return self.identifier

    @staticmethod
    def matching(value: str) -> Callable[["Aliment"], bool]:
        value = value.lower()

        def predicate(aliment: "Aliment") -> bool:
            return value in aliment.label.lower() or value in aliment.note.lower()

        return predicate


@dataclass
class Pantry:
    aliments: Dict[str, Aliment]

    @staticmethod
    def empty() -> "Pantry":
        return Pantry({})

    @staticmethod
    def filled(*aliments: Aliment) -> "Pantry":
        return Pantry(dict((item.key(), item) for item in aliments))

    @staticmethod
    def create(aliments: Iterable[Aliment]) -> "Pantry":
        return Pantry(dict((item.label, item) for item in aliments))

    def filter(self, predicate: Callable[[Aliment], bool]) -> List[Aliment]:
        return [aliment for aliment in self.aliments.values() if predicate(aliment)]

    def add(self, aliment: Aliment) -> None:
        self.aliments[aliment.key()] = aliment

    def __call__(self, key: str, qty: Quantity) -> Ingredient:
        if key not in self.aliments:
            raise ValueError("not found: " + key)
        i = self.aliments[key]
        return Ingredient(i.label, qty, i.group, i.composition_for(qty))

    def __add__(self, other: "Pantry") -> "Pantry":
        return Pantry.filled(
            *(list(self.aliments.values()) + list(other.aliments.values()))
        )
