# -*- coding: utf-8 -*-
from typing import Callable

from recipes.composition import Composition
from recipes.pantry import Pantry, Aliment
from recipes.quantity import Quantity


Action = Callable[[Pantry], None]
AlimentGetter = Callable[[Pantry], Aliment]


def pantry(*actions: Action) -> "Pantry":
    pantry = Pantry.empty()
    for action in actions:
        action(pantry)
    return pantry


def aliment(
    identifier: str, label: str, group: str, note: str, composition: Composition
) -> Action:
    def process(pantry: Pantry) -> None:
        pantry.add(Aliment(identifier, label, group, note, composition))

    return process


def derived_aliment(identifier: str, label: str, content: AlimentGetter) -> Action:
    def process(pantry: Pantry) -> None:
        source = content(pantry)
        pantry.add(
            Aliment(identifier, label, source.group, source.note, source.composition)
        )

    return process


def of(identifier: str) -> AlimentGetter:
    # pylint: disable=invalid-name
    def get(pantry: Pantry) -> Aliment:
        return pantry.aliments[identifier]

    return get


def dehydrated(getter: AlimentGetter) -> AlimentGetter:
    def get(pantry: Pantry) -> Aliment:
        inner = getter(pantry)
        return inner.dehydrated()

    return get


def composition(**kwargs: Quantity) -> Composition:
    return Composition.for100g(**kwargs)


def value(definition: str) -> Quantity:
    return Quantity.parse(definition)


def missing() -> Quantity:
    return Quantity.none()
